import { defineConfig } from 'astro/config';

export default defineConfig({
    site: 'https://dian-kaagman.gitlab.io',
    base: '/resume',
    outDir: 'public',
    publicDir: 'static',
  });
